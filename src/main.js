import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router';

import routes from '../src/router';

const router = new Router({ routes });

Vue.use(Router)

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
