import axios from 'axios';
import configService from './config';

const loginService = {};

loginService.logeo = function (operacion, body) {
    //bibliotecaService.post('/loginUser/login', {usuario});
    let request = {
        operacion: operacion,
        body: {
            codigo: body.codigo,
            password: body.password
        }
    }
    return axios.post(configService.apiUrl+'/loginUser/login', {request});
}

export default loginService;