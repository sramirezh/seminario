import Login from '../src/components/Login.vue';
import Home from '../src/components/Home.vue';
import Reserva from '../src/components/Reserva.vue';
import Usuario from '../src/components/Usuario.vue';

const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/reserva',
        name: 'reserva',
        component: Reserva
    },
    {
        path: '/usuario',
        name: 'usuario',
        component: Usuario
    }
]

export default routes;